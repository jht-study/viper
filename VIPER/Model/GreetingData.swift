//
//  GreetingData.swift
//  VIPER
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

// Transport data structure (not Entity)
struct GreetingData {
    let greeting: String
    let subject: String
}
