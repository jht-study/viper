//
//  GreetingInteractor.swift
//  VIPER
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

class GreetingInteractor: GreetingProvider{
    weak var output: GreetingOutput!
    
    func provideGreetingData() {
        let person = Person(firtName: "Jeferson", lastName: "Takumi")
        let subject = person.firtName + " " + person.lastName
        let greeting = GreetingData(greeting: "Hello", subject: subject)
        self.output.receiveGreetingData(greetingData: greeting)
    }
}
