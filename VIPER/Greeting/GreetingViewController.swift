//
//  GreetingViewController.swift
//  VIPER
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import UIKit

class GreetingViewController: UIViewController, GreetingView {
    var eventHandler: GreetingViewEventHandler!
    @IBOutlet var showGreetingButton: UIButton!
    @IBOutlet var greetingLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showGreetingButton.addTarget(self, action: #selector(didTapButton(button:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let presenter = GreetingPresenter()
        let interactor = GreetingInteractor()
        presenter.view = self
        presenter.greetingProvider = interactor
        interactor.output = presenter
        
        eventHandler = presenter
    }
    
    @objc func didTapButton(button: UIButton) {
        self.eventHandler.didTabShowGreetingButton()
    }
    
    func setGreeting(greeting: String) {
        self.greetingLabel.text = greeting
    }
}
