//
//  GreetingView.swift
//  VIPER
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

protocol GreetingView: class {
    func setGreeting(greeting: String)
}
